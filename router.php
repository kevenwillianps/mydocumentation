<?php

    /**
     * Created by MyCode
     * User: KEVEN
     * Date: 31/07/2019
     * Time: 13:32
     **/

    /** Instância da classe Main **/
    include_once ('lib/class/Main.class.php');

    /** Construtor **/
    $Main   = new Main();

    /** Requisições **/
    $TABLE  = isset($_REQUEST['TABLE'])  ? htmlspecialchars($_REQUEST['TABLE'])  : '';
    $ACTION = isset($_REQUEST['ACTION']) ? htmlspecialchars($_REQUEST['ACTION']) : '';

    /** Verifico a tabela **/
    switch ($TABLE)
    {

        /** Vejo minhas rotas disponíveis para a tabela 'GN_CONTACT' **/
        case 'GN_CONTACT':

            /** Rota para carregar o formulário **/
            if($ACTION == 'SAVE')
            {

                include('action/GnContact/gn_contact_save.php');

            }
            break;

    }
