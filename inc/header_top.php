<nav class="navbar navbar-expand-md navbar-dark bg-primary-sticky">

    <div class="container">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#hedaer_top" aria-controls="hedaer_top" aria-expanded="false" aria-label="Toggle navigation">

            <span class="navbar-toggler-icon"></span>

        </button>

        <div class="container">

            <div class="collapse navbar-collapse text-center justify-content-center" id="hedaer_top">

                <ul class="navbar-nav">

                    <li>

                        <a href="<?php echo $Organization->complement?>" data-content="#" class="nav-link wow fadeIn" data-wow-delay="0.2s">

                            <i class="fas fa-home"></i>  <?php echo utf8_encode($Organization->complement)?>

                        </a>

                    </li>

                    <li class="nav-item mx-2">

                        <a href="tel:<?php echo $Organization->main_phone?>" data-content="#" class="nav-link wow fadeIn" data-wow-delay="0.4s">

                            <i class="fas fa-phone"></i> <?php echo utf8_encode($Organization->main_phone)?>

                        </a>

                    </li>

                    <li class="nav-item mx-2">

                        <a href="mailto:<?php echo $Organization->email?>" data-content="#" class="nav-link wow fadeIn" data-wow-delay="0.6s">

                            <i class="fas fa-envelope"></i> <?php echo utf8_encode($Organization->email)?>

                        </a>

                    </li>

                </ul>

            </div>

        </div>

    </div>

</nav>