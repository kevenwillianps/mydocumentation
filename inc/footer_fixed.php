<footer class="col-md-12 text-white fixed-bottom">

    <div class="container pt-4">

        <div class="row">

            <div class="col-md-4">

                <div class="media">

                    <div class="media-body text-justify">

                        <h4 class="mt-0">

                            <strong>Nos encontre</strong>

                        </h4>

                        <div class="mt-3">

                            <div class="media mt-2">

                                <i class="fas fa-map-signs align-self-center mr-3"></i>

                                <div class="media-body">

                                    <?php echo utf8_encode($Organization->complement)?>

                                </div>

                            </div>

<!--                            <div class="media mt-2">-->
<!---->
<!--                                <i class="fas fa-envelope align-self-center mr-3"></i>-->
<!---->
<!--                                <div class="media-body">-->
<!---->
<!--                                    --><?php //echo @utf8_encode($General->email)?>
<!---->
<!--                                </div>-->
<!---->
<!--                            </div>-->

                            <div class="media mt-2">

                                <i class="fas fa-phone align-self-center mr-3"></i>

                                <div class="media-body">

                                    <?php echo utf8_encode($Organization->main_phone)?>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="col-md-4">

                <div class="media">

                    <div class="media-body text-justify">

                        <h4 class="mt-0">

                            <strong>Links Rápidos</strong>

                        </h4>

                        <div class="mt-3">

                            <?php

                            /** Chamo o método que traz todos os registros **/
                            $Content->All($organization_id);
                            while($rowContent = $Content->FetchObject()){ ?>

                                <?php if((!$rowContent->banner) && (!$rowContent->accordion) && (!$rowContent->video) && (!$rowContent->link) && (!$rowContent->home) && ($rowContent->quick_link)){?>

                                    <a href="<?php echo $rowContent->url?>" class="text-decoration-none text-white">

                                        <?php echo utf8_encode($rowContent->title)?> <br>

                                    </a>

                                <?php }?>

                            <?php }?>

                        </div>

                    </div>

                </div>

            </div>

            <div class="col-md-4">

                <div class="media">

                    <div class="media-body">

                        <h4 class="mt-0">

                            <strong>Siga-nos</strong>

                        </h4>

                        <div class="mt-3">

                            <h3>

                                <a href="<?php echo @utf8_encode($Organization->facebook)?>" class="text-decoration-none text-white">

                                    <i class="fab fa-facebook"></i>

                                </a>

                            </h3>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="row">

        <div class="col-md-12 mt-4 pt-3 pb-2 text-center text-white-50 strip">

            <h5>

                &copy; Copyright <?php echo date('Y');?> - <?php echo @utf8_encode($Organization->copyright)?>

            </h5>

        </div>

    </div>

</footer>