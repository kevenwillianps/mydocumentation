<section>

    <main>

        <div class="container my-5">

            <h1 class="text-center">

                Erro! Página Não econtrada

            </h1>

            <p class="zoom-area">

                <b>

                    Atenção!

                </b>

                Tente novamente dentro de alguns minutos

            </p>

            <section class="error-container">
                <span class="four"><span class="screen-reader-text">4</span></span>
                <span class="zero"><span class="screen-reader-text">0</span></span>
                <span class="four"><span class="screen-reader-text">4</span></span>
            </section>

        </div>

    </main>

</section>