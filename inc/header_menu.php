<nav class="navbar navbar-expand-lg navbar-sticky sticky-top navbar-light bg-light">

    <div class="container">

        <a href="<?php echo utf8_encode($General->url)?>" class="navbar-brand">

            <img src="img/logo.png" width="150px" alt="<?php echo utf8_encode($Organization->social_name) ?> - <?php echo utf8_encode($Organization->fantasy_name) ?>">

        </a>

        <a href="<?php echo utf8_encode($General->url)?>" class="navbar-brand">

            <img src="img/SELOS_PQTA_2018_OURO.gif" width="40px" alt="<?php echo utf8_encode($Organization->social_name) ?> - <?php echo utf8_encode($Organization->fantasy_name) ?>">

        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">

            <span class="navbar-toggler-icon"></span>

        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <ul class="navbar-nav">

                <?php

                $delayMenu = 0;

                /** Chamo o método que traz todos os registros **/
                $Content->All($organization_id);
                while($rowContent = $Content->FetchObject()){ ?>

                    <?php if (($ContentSub->Count($rowContent->content_id)) == 0){?>

                        <?php if ((!empty($rowContent->title_menu))){ ?>

                            <li class="nav-item wow fadeIn" data-wow-delay="0.<?php echo $delayMenu?>s">

                                <a href="content/<?php echo $rowContent->content_id?>/<?php echo $rowContent->title?>" class="nav-link">

                                    <?php echo utf8_encode($rowContent->title_menu)?>

                                </a>

                            </li>

                        <?php }?>

                    <?php }else{?>

                        <?php if ((!empty($rowContent->title_menu)) && (strcmp('no dropdown', $rowContent->highlighter) == 0)){ ?>

                            <li class="nav-item wow fadeIn" data-wow-delay="0.<?php echo $delayMenu?>s">

                                <a href="content/<?php echo $rowContent->content_id?>/<?php echo $rowContent->title?>" class="nav-link">

                                    <?php echo utf8_encode($rowContent->title_menu)?>

                                </a>

                            </li>

                        <?php }elseif((!empty($rowContent->title_menu)) && (strcmp('dropdown', $rowContent->highlighter) == 0)){?>


                            <li class="nav-item dropdown wow fadeIn" data-wow-delay="0.<?php echo $delayMenu?>s">

                                <a href="#" class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                    <?php echo utf8_encode($rowContent->title_menu)?>

                                </a>

                                <div class="dropdown-menu animate slideIn" aria-labelledby="navbarDropdown">

                                    <?php

                                    /** Chamo o método que traz todos os registros **/
                                    $ContentSub->Load($rowContent->content_id);
                                    while($rowContentSub = $ContentSub->FetchObject()){ ?>

                                        <?php if (!empty($rowContentSub->title_menu)){ ?>

                                            <a href="content_sub/<?php echo $rowContentSub->content_sub_id?>/<?php echo utf8_encode($rowContentSub->title_menu)?>" class="dropdown-item">

                                                <?php echo utf8_encode($rowContentSub->title_menu)?>

                                            </a>

                                        <?php }?>

                                    <?php }?>

                                </div>

                            </li>

                        <?php }?>

                    <?php }?>

                <?php $delayMenu++;}?>

            </ul>

        </div>

    </div>

</nav>