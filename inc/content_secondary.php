<section id="welcome">

    <main>

        <div class="jumbotron text-justify">

            <div class="container">

                <h1 class="display-4 wow fadeInUp" data-wow-delay="0.2s">

                    <?php

                        /** Exibo o Título do conteúdo **/
                        echo utf8_encode($rowContent->title)

                    ?>

                </h1>

                <p class="lead wow fadeInUp" data-wow-delay="0.6s">

                    <?php

                        /** Exibo o Conteúdo Resumido **/
                        echo utf8_encode($rowContent->content_resume)

                    ?>

                </p>

                <hr class="my-4 wow fadeInUp" data-wow-delay="1.0s">

                <div class="wow fadeIn" data-wow-delay="0.8s">

                    <?php

                        /** Exibo o Conteúdo Completo **/
                        echo utf8_encode($rowContent->content_complete)

                    ?>

                </div>

            </div>

        </div>

        <div class="container" style="margin-top: -70px">

            <?php

            $delayContentSub = 2;

            /** Chamo o método que traz todos os registros **/
            $ContentSub->Load($rowContent->content_id);
            while($rowContentSub = $ContentSub->FetchObject()){ ?>

                <div type="button" class="card shadow-sm mb-3 wow fadeInUp" data-wow-delay="0.<?php echo $delayContentSub?>s" data-toggle="collapse" href="#collapse_<?php echo $rowContentSub->content_sub_id?>" role="button" aria-expanded="false" aria-controls="collapse_<?php echo $rowContentSub->content_sub_id?>">

                    <div class="card-body">

                        <h4 class="card-title">

                            <?php

                                /** Exibo o título do sub-conteúdo **/
                                echo utf8_encode($rowContentSub->title)

                            ?>

                        </h4>

                        <h6 class="card-subtitle mb-2 text-muted">

                            Clique para expandir

                        </h6>

                        <div class="collapse" id="collapse_<?php echo $rowContentSub->content_sub_id?>">

                            <hr>

                            <?php

                                /** Exibo o conteúdo completo do sub-conteúdo **/
                                echo utf8_encode($rowContentSub->content_complete)

                            ?>

                        </div>

                    </div>

                </div>

            <?php $delayContentSub++;}?>

        </div>

    </main>

</section>