<section id="welcome">

    <main>

        <?php

        /** Verifico se possui banner para determinada organização **/
        if ($Content->CountBanner($organization_id) > 0){ ?>

            <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">

                <ol class="carousel-indicators">

                    <?php

                    /** Contador de indicadores **/
                    $countIndicator = 0;

                    /** Chamo o método que traz todos os registros **/
                    $Content->Banner($organization_id);
                    while($rowContent = $Content->FetchObject()){ ?>

                        <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $countIndicator?>" class="<?php echo $countIndicator == 0 ? "active" : "" ?>"></li>

                    <?php $countIndicator++;}?>

                </ol>

                <div class="carousel-inner">

                    <?php

                    /** Contador de itens ativos **/
                    $countActive = 0;

                    /** Chamo o método que traz todos os registros **/
                    $Content->Banner($organization_id);
                    while($rowContent = $Content->FetchObject()){ ?>

                        <div class="carousel-item <?php echo $countActive == 0 ? "active" : "" ?>">

                            <img src="<?php echo $repository_images . $organization_id?>/content_sub/<?php echo $rowContent->content_sub_id?>/<?php echo $rowContent->name?>" class="d-block w-100" alt="<?php echo $Organization->social_name ?> - <?php echo $Organization->fantasy_name ?>">

                        </div>

                    <?php $countActive++;}?>

                </div>

                <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">

                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">

                        Voltar

                    </span>

                </a>

                <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">

                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">

                        Avançar

                    </span>

                </a>

            </div>

        <?php }?>

        <?php

        /** Chamo o método que traz todos os registros **/
        $Content->All($organization_id);
        while($rowContent = $Content->FetchObject()){ ?>

            <?php

            /** Verifico o marcador **/
            if (($rowContent->highlighter == 'jumbotron2') && (($ContentSub->Count($rowContent->content_id)) > 0)){?>

                <div class="jumbotron" id="content_<?php echo $rowContent->content_id?>">

                    <div class="container">

                        <h1 class="display-4 wow fadeInUp" data-wow-delay="0.2s">

                            <?php

                                /** Exibo o título do conteúdo **/
                                echo utf8_encode($rowContent->title)

                            ?>

                        </h1>

                        <p class="lead wow fadeInUp" data-wow-delay="0.4s">

                            <?php

                                /** Exibo o conteúdo resumido **/
                                echo utf8_encode($rowContent->content_resume)

                            ?>

                        </p>

                        <hr class="my-4 wow fadeInUp" data-wow-delay="0.6s">

                        <div class="owl-carousel owl-theme">

                            <?php

                            $delayCarousel = 0;

                            /** Chamo o método que traz todos os registros **/
                            $ContentSub->Load($rowContent->content_id);
                            while($rowContentSub = $ContentSub->FetchObject()){ ?>

                                <div class="item wow fadeIn" data-wow-delay="0.<?php echo $delayCarousel?>s">

                                    <div class="card">

                                        <div class="card-body">

                                            <img src="<?php echo $repository_images . $organization_id?>/content_sub/<?php echo $rowContentSub->content_sub_id?>/<?php echo $rowContentSub->name?>" class="rounded mx-auto d-block hvr-bounce-in" style="width: 50px !important;" alt="<?php echo $Organization->social_name ?> - <?php echo $Organization->fantasy_name ?>">

                                            <h5 class="card-title text-center mt-3">

                                                <?php

                                                    /** Exibo o título do conteúdo **/
                                                    echo utf8_encode($rowContentSub->title)

                                                ?>

                                            </h5>

                                            <h6 class="card-subtitle mb-2 text-muted text-center">

                                                <?php

                                                    /** Exibo a observação do conteúdo **/
                                                    echo utf8_encode($rowContentSub->content_observation)

                                                ?>

                                            </h6>

                                            <p class="card-text text-center mt-3">

                                                <?php

                                                    /** Exibo o conteúdo completo **/
                                                    echo utf8_encode($rowContentSub->content_complete)

                                                ?>

                                            </p>

                                        </div>

                                    </div>

                                </div>

                                <?php $delayCarousel++;}?>

                        </div>

                    </div>

                </div>

            <?php }?>

        <?php }?>

        <?php

        /** Chamo o método que traz todos os registros **/
        $Content->All($organization_id);
        while($rowContent = $Content->FetchObject()){ ?>

            <?php

            /** Verifico o marcador **/
            if($rowContent->highlighter == 'find'){?>

                <div class="container" id="content_<?php echo $rowContent->content_id?>">

                    <div class="row">

                        <div class="col-md-6 text-justify">

                            <h1 class="display-4 wow fadeInUp" data-wow-delay="0.2s">

                                <?php

                                /** Exibo o título do conteúdo **/
                                echo utf8_encode($rowContent->title)

                                ?>

                            </h1>

                            <p class="lead wow fadeInUp" data-wow-delay="0.6s">

                                <?php

                                /** Exibo o conteúdo completo **/
                                echo utf8_encode($rowContent->content_complete)

                                ?>

                            </p>

                            <hr class="my-4 wow fadeInUp" data-wow-delay="0.8s">

                            <ul class="list-unstyled">

                                <li class="media wow fadeInLeft" data-wow-delay="0.2s">

                                    <i class="fas fa-map-marker-alt align-self-center mr-3"></i>

                                    <div class="media-body">

                                        <p>

                                            <?php echo utf8_encode($Organization->city)?>/<?php echo utf8_encode($Organization->uf)?>, <?php echo utf8_encode($Organization->complement)?> - CEP: <?php echo utf8_encode($Organization->zip_code)?>

                                        </p>

                                    </div>

                                </li>

                                <hr class="wow fadeInUp" data-wow-delay="0.6s">

                                <li class="media my-4 wow fadeInRight" data-wow-delay="0.4s">

                                    <i class="fas fa-mobile-alt align-self-center mr-3"></i>

                                    <div class="media-body">

                                        <?php echo utf8_encode($Organization->main_phone)?>

                                    </div>

                                </li>

                                <hr class="wow fadeInUp" data-wow-delay="0.6s">

                                <li class="media my-4 wow fadeInLeft" data-wow-delay="0.6s">

                                    <i class="fas fa-envelope align-self-center mr-3"></i>

                                    <div class="media-body">

                                        <?php echo utf8_encode($Organization->email)?>

                                    </div>

                                </li>

                            </ul>

                        </div>

                        <div class="col-md-6">

                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3820.6878658375676!2d-49.2791483856718!3d-16.742418988468096!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef0b12269ad25%3A0xf73d4eac09f4a03d!2sE-Business+Rio+Verde!5e0!3m2!1spt-BR!2sbr!4v1565725706486!5m2!1spt-BR!2sbr" height="600px" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>

                        </div>

                    </div>

                </div>

            <?php }?>

        <?php }?>

        <?php

        /** Chamo o método que traz todos os registros **/
        $Content->All($organization_id);
        while($rowContent = $Content->FetchObject()){ ?>

            <?php

            /** Verifico o marcador **/
            if($rowContent->highlighter == 'contact'){?>

                <div class="jumbotron jumbotron-secondary contact" id="content_<?php echo $rowContent->content_id?>">

                    <div class="container">

                        <h1 class="display-4 wow fadeInUp" data-wow-delay="0.2s">

                            <?php

                                /** Exibo o título do conteúdo **/
                                echo utf8_encode($rowContent->title)

                            ?>

                        </h1>

                        <p class="lead wow fadeInUp" data-wow-delay="0.4s">

                            <?php

                                /** Exibo o conteúdo completo **/
                                echo utf8_encode($rowContent->content_complete)

                            ?>

                        </p>

                        <hr class="my-4 wow fadeInUp" data-wow-delay="0.6s">

                        <div class="card wow fadeInDown" data-wow-delay="0.2s">

                            <div class="card-body">

                                <?php

                                    $delayContentSub = 0;

                                    /** Chamo o método que traz todos os registros **/
                                    $ContentSub->Load($rowContent->content_id);
                                    while($rowContentSub = $ContentSub->FetchObject()){ ?>

                                        <div class="alert alert-info">

                                            <h5>

                                                <i class="fas fa-info"></i>

                                                <?php

                                                    /** Exibo o título do conteúdo **/
                                                    echo utf8_encode($rowContentSub->title)

                                                ?>

                                            </h5>

                                            <ul>

                                                <?php

                                                    /** Exibo o título do conteúdo **/
                                                    echo utf8_encode($rowContentSub->content_complete)

                                                ?>

                                            </ul>

                                        </div>

                                    <?php $delayContentSub++;}?>

                                <form role="form" id="FrmGnContact">

                                    <div class="row">

                                        <div class="col-md-6">

                                            <div class="form-group">

                                                <input type="text" class="form-control" placeholder="Nome" name="name">

                                            </div>

                                        </div>

                                        <div class="col-md-6">

                                            <div class="form-group">

                                                <input type="text" class="form-control" placeholder="Email" name="email">

                                            </div>

                                        </div>

                                        <div class="col-md-6">

                                            <div class="form-group">

                                                <input type="text" class="form-control" placeholder="Telefone" name="telephone">

                                            </div>

                                        </div>

                                        <div class="col-md-6">

                                            <div class="form-group">

                                                <input type="text" class="form-control" placeholder="Celular" name="cellphone">

                                            </div>

                                        </div>

                                        <div class="col-md-12">

                                            <div class="form-group">

                                                <select name="subject_matter" id="subject_matter" class="custom-select">

                                                    <option value="">Selecione um assunto</option>
                                                    <option value="contato">Contato</option>
                                                    <option value="elogio">Elogios</option>
                                                    <option value="sugestao">Sugestões</option>
                                                    <option value="denuncia">Denúncia</option>

                                                </select>

                                            </div>

                                        </div>

                                        <div class="col-md-12">

                                            <div class="form-group">

                                                <textarea name="message_contact" id="message_contact" rows="5" class="form-control" placeholder="Mensagem"></textarea>

                                            </div>

                                        </div>

                                        <div class="col-md-12 text-right">

                                            <div class="form-group">

                                                <button type="button" class="btn btn-outline-primary" onclick="sendForm('#FrmGnContact')">

                                                    Enviar <i class="fas fa-paper-plane"></i>

                                                </button>

                                            </div>

                                        </div>

                                        <input type="hidden" name="TABLE" value="GN_CONTACT">
                                        <input type="hidden" name="ACTION" value="SAVE">
                                        <input type="hidden" name="ORGANIZATION_ID" value="<?php echo $organization_id?>">

                                    </div>

                                </form>

                            </div>

                        </div>

                    </div>

                </div>

            <?php }?>

        <?php }?>

    </main>

</section>