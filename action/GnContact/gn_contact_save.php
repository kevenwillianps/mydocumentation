<?php
try{

   /**
    * Created by MyCode
    * User: KEVEN
    * Date: 31/07/2019
    * Time: 13:32
   **/

   /** Instânciamento da classe GnContact  **/
   $GnContact       = $Main->LoadClass('GnContact');

   /** Parâmetros de entrada **/
   $contact_id      = isset($_POST['contact_id'])      ? (int)$Main->anti_injection($_POST['contact_id'])         : 0;
   $organization_id = isset($_POST['ORGANIZATION_ID']) ? (int)$Main->anti_injection($_POST['ORGANIZATION_ID'])    : 0;
   $situation_id    = isset($_POST['situation_id'])    ? (int)$Main->anti_injection($_POST['situation_id'])       : 1;
   $name            = isset($_POST['name'])            ? (string)$Main->anti_injection($_POST['name'])            : '';
   $email           = isset($_POST['email'])           ? (string)$Main->anti_injection($_POST['email'])           : '';
   $telephone       = isset($_POST['telephone'])       ? (string)$Main->anti_injection($_POST['telephone'])       : '';
   $cellphone       = isset($_POST['cellphone'])       ? (string)$Main->anti_injection($_POST['cellphone'])       : '';
   $subject_matter  = isset($_POST['subject_matter'])  ? (string)$Main->anti_injection($_POST['subject_matter'])  : '';
   $message_contact = isset($_POST['message_contact']) ? (string)$Main->anti_injection($_POST['message_contact']) : '';
   $date_register   = isset($_POST['date_register'])   ? (string)$Main->anti_injection($_POST['date_register'])   : date("y-m-d h:m:s");
   $date_update     = isset($_POST['date_update'])     ? (string)$Main->anti_injection($_POST['date_update'])     : date("y-m-d h:m:s");
   $error           = '';
   $message         = '';

   /** Validação de campos obrigatórios **/
   /** Verifico se o campo contact_id foi preenchido **/
   if($contact_id < 0){

       $error++;
       $message .= $error. ' - O deve ser preenchido/selecionado <strong>contact_id</strong>.<br/>';

   }
   /** Verifico se o campo organization_id foi preenchido **/
   if($organization_id <= 0){

       $error++;
       $message .= $error. ' - O deve ser preenchido/selecionado <strong>organization_id</strong>.<br/>';

   }
   /** Verifico se o campo situation_id foi preenchido **/
   if($situation_id < 0){

       $error++;
       $message .= $error. ' - O deve ser preenchido/selecionado <strong>situation_id</strong>.<br/>';

   }
   /** Verifico se o campo name foi preenchido **/
   if(empty($name)){

       $error++;
       $message .= $error. ' - <strong>Nome</strong>: O deve ser preenchido/selecionado.<br/>';

   }
   /** Verifico se o campo email foi preenchido **/
   if(empty($email)){

       $error++;
       $message .= $error. ' -  <strong>Email</strong>: O deve ser preenchido/selecionado.<br/>';

   }
   /** Verifico se o campo telephone foi preenchido **/
   if(empty($telephone)){

       $error++;
       $message .= $error. ' - <strong>Telefone</strong>: O deve ser preenchido/selecionado.<br/>';

   }
   /** Verifico se o campo cellphone foi preenchido **/
   if(empty($cellphone)){

       $error++;
       $message .= $error. ' -  <strong>Celular</strong>:O deve ser preenchido/selecionado.<br/>';

   }
   /** Verifico se o campo subject_matter foi preenchido **/
   if(empty($subject_matter)){

       $error++;
       $message .= $error. ' - <strong>Assunto</strong>: O deve ser preenchido/selecionado.<br/>';

   }
   /** Verifico se o campo message foi preenchido **/
   if(empty($message_contact)){

       $error++;
       $message .= $error. ' -  <strong>Mensagem</strong>: O deve ser preenchido/selecionado.<br/>';

   }
   /** Verifico se o campo date_register foi preenchido **/
   if(empty($date_register)){

       $error++;
       $message .= $error. ' - O deve ser preenchido/selecionado <strong>date_register</strong>.<br/>';

   }
   /** Verifico se o campo date_update foi preenchido **/
   if(empty($date_update)){

       $error++;
       $message .= $error. ' - O deve ser preenchido/selecionado <strong>date_update</strong>.<br/>';

   }

   if($error > 0){

       /** Preparo o formulario para retorno **/
       $result = array("cod"   => 0,
                       "msg"   => $message." <br/> Total de erros encontrados: ".$error,
                       "title" => "Atenção");

       /**pause*/
       sleep(1);

       /**Envio*/
       echo json_encode($result);

       /**Para o procedimento*/
       exit;

   }else{

       $GnContact->Save($contact_id, $organization_id, $situation_id, $name, $email, $telephone, $cellphone, $subject_matter, $message_contact, $date_register, $date_update);

       /** Preparo o formulario para retorno **/
       $result = array("cod"   => 0,
                       "msg"   => "Mensagem enviada com sucesso",
                       "title" => "Atenção");

       /** Pause **/
       sleep(1);

       /** Envio **/
       echo json_encode($result);

       /** Paro o procedimento **/
       exit;

   }

}catch(Exception $e){

   /** Preparo o formulario para retorno **/
   $result = array("cod"   => 0,
                   "msg"   => $e->getMessage(),
                   "title" => "Atenção");

   /** Pause **/
   sleep(1);

   /** Envio **/
   echo json_encode($result);

   /** Paro o procedimento **/
   exit;

}
