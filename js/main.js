function sendForm(form) {

    $.ajax({

        url      : "router.php",
        type     : "post",
        dataType : "json",
        data     : $(form).serialize(),

        beforeSend : function () {

            var div  = '<div class="modal hide fade in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
                div += '	<div class="modal-dialog">';
                div += '		<div class="modal-content">';
                div += '			<div class="modal-header">';
                div += '		        <h5>Processando</h5>';
                div += '		    </div>';
                div += '			<div class="modal-body">';
                div += '                <div class="progress">';
                div += '                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>';
                div += '                </div>';
                div += '			</div>';
                div += '		</div>';
                div += '	</div>';
                div += '</div>';

            /** Carrego o popup **/
            $('body').append(div);

            /** Abro o popup **/
            $('#myModal').modal('show');

        },

        success: function (answer) {

            /** Abro um popup com os dados **/
            openPopup(answer.title, answer.msg);

        },

        error: function (xhr, ajaxOptions, thrownError) {

            /** Abro um popup com os dados **/
            openPopup('Atenção', xhr.status + ' - ' + ajaxOptions + ' - ' + thrownError);

        }

    });

}

function openPopup(title, msg) {

    /** Oculto o popup anterior **/
    $('#myModal').modal('dispose');
    $('#myModal').remove();
    $('.modal-backdrop').remove();
    $('nav').removeAttr('style');
    $('footer').removeAttr('style');

    var div  = '<div class="modal hide fade in" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
        div += '	<div class="modal-dialog">';
        div += '		<div class="modal-content">';
        div += '			<div class="modal-header">';
        div += '                <h4 class="modal-title" id="myModalLabel">' + title + '</h4>';
        div += '                <button type="button" class="close" data-dismiss="modal">&times;</button>';
        div += '            </div>';
        div += '            <div class="modal-body">';
        div +=                  msg;
        div += '            </div>';
        div += '            <div class="modal-footer">';
        div += '                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>';
        div += '            </div>';
        div += '        </div>';
        div += '    </div>';
        div += '</div>';

    /** Carrego o popup **/
    $('body').append(div);

    /** Abro o popup **/
    $('#myModal2').modal('show');

}