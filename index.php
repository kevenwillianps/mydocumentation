<?php

    /** Instânciamento da classe 'Main' **/
    include_once('lib/class/Main.class.php');
    /** Método construtor **/
    $Main            = new Main();
    /** Defino o ID da Organização **/
    $organization_id = 31;
    /** Defino o repositório de imagens da Organização **/
    $repository_images = 'http://mycms.softwiki.com.br/images/organization_';
    /** Instânciamento da classe 'General' de acordo com o ID da Organização **/
    $General         = $Main->LoadClass('General')->Get($organization_id);
    /** Instânciamento da classe 'Content' **/
    $Content         = $Main->LoadClass('Content');
    /** Instânciamento da classe 'ContentSub' **/
    $ContentSub      = $Main->LoadClass('ContentSub');
    /** Instânciamento da classe 'Organization' de acordo com o ID da Organização **/
    $Organization    = $Main->LoadClass('GnOrganization')->Get($organization_id);

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>

    <base href="<?php echo utf8_encode($General->url)?>"/>
    <meta charset="<?php echo utf8_encode($General->charsert);?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-language" content="PT" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta name="resource-types" content="document" />
    <meta http-equiv="content-language" content="PT" />
    <meta name="revisit-after" content="1" />
    <meta name="classification" content="Internet" />
    <meta name="robots" content="index,follow" />
    <meta name="distribution" content="Global" />
    <meta name="rating" content="General" />
    <meta name="audience" content="all" />
    <meta name="language" content="pt-br" />
    <meta name="doc-class" content="Completed" />
    <meta name="doc-rights" content="Public" />
    <meta name="revisit-after" content="1 days" />
    <meta name="title" content="<?php echo utf8_encode($General->fantasy_name);?>" />
    <meta name="copyright" content="<?php echo utf8_encode($General->copyright);?>" />
    <meta name="googlebot" content="index, follow" />
    <link rev=made href="mailto:<?php echo utf8_encode($General->email);?>"/>
    <meta name="description" content="<?php echo @utf8_encode($General->description);?>">
    <meta name="author" content="<?php echo utf8_encode($General->author);?>">
    <meta name="keywords" content="<?php echo utf8_encode($General->keywords);?>"/>
    <link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />

    <title>

        <?php echo utf8_encode($General->fantasy_name);?>

    </title>

    <!-- Importo os arquivos css -->
    <link rel="stylesheet" href="css/normalize.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="font/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/component.css">
    <link rel="stylesheet" href="css/hover.min.css">
    <link rel="stylesheet" href="css/error.css">
    <link rel="stylesheet" href="css/animatedropdown.css">
    <!-- Importo os arquivos js -->
    <script type="text/javascript" src='js/jquery.min.js'></script>
    <script type="text/javascript" src="js/popper.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>

</head>

<body>

    <div class="supreme-container">

        <?php

            /** Carrego o menu que fica fixo no topo da página **/
            include('inc/header_top.php');

        ?>

        <?php

            /** Carrego o menu que acompanha a página **/
            include('inc/header_menu.php');

        ?>

        <?php

            /** Verifico qual layout deve ser carregado para exibir o conteúdo **/
            switch ($_REQUEST['highlighter']){

                /** Exibição principal **/
                case 'index' :

                    include('inc/content.php');
                    break;

                /** Exibição para conteúdo **/
                case 'content' :

                    /** Parâmetros de entrada **/
                    $content_id = isset($_REQUEST['content_id']) ? ((int)$_REQUEST['content_id']) : 0;
                    /** Listo um registro especifíco **/
                    $rowContent = $Content->Get($content_id);

                    /** Verifico se o registro foi encontrdao **/
                    if (isset($rowContent)){

                        include('inc/content_secondary.php');

                    }else{

                        include('inc/content_error.php');

                    }
                    break;

                /** Exibição para sub-conteúdo **/
                case 'content_sub' :

                    /** Parâmetros de entrada **/
                    $content_sub_id = isset($_REQUEST['content_sub_id']) ? ((int)$_REQUEST['content_sub_id']) : 0;

                    /** Listo um registro especifíco **/
                    $rowContentSub  = $ContentSub->Get($content_sub_id);

                    /** Verifico se o registro foi encontrdao **/
                    if (isset($rowContentSub)){

                        include('inc/content_sub_secondary.php');

                    }else{

                        include('inc/content_error.php');

                    }
                    break;

                /** Exibição de erro caso não econtre nenhum página **/
                default :

                    include('inc/content_error.php');
                    break;

            }

        ?>

        <?php

            /** Importo o rodapé **/
            include('inc/footer_fixed.php');

        ?>

    </div>

    <script type="text/javascript">

        $('.owl-carousel').owlCarousel({

            loop       : true,
            margin     : 10,
            nav        : false,
            responsive :{

                0:{

                    items:1

                },

                600:{

                    items:3

                },

                1000:{

                    items:3

                }

            }

        });

    </script>

    <script type="text/javascript">

        new WOW().init();

    </script>

</body>
</html>