<?php
/**
* Classe gn_content.class.php
* @filesource
* @autor		Kenio de Souza
* @copyright	Copyright 2017 Serenity Informatica
* @package		class
* @subpackage	class.class
* @version		1.0
* Total de campos para essa classe [12]*/

/** Configuração de acesso ao banco de dados */
require_once("lib/class/Host.php");

/** Classe de conexão ao banco de dados Mysql */
require_once("lib/class/Mysql.class.php");

class Content
{

    /** Construtor da classe */
    function __construct()
    {

        /** Cria o objeto de conexão com o banco de dados **/
        $this->obj = new Connect;

    }

    /** Lista todos os egistros do banco com ou sem paginação*/
    function All($organization_id)
    {

        /** Parâmetros de entrada **/
        $this->organization_id = (int)$organization_id;

        /** Consulta SQL **/
        $sql = "SELECT * FROM gn_content
                WHERE organization_id = {$this->organization_id}";

        /** Executo o comando SQL */
        $this->obj->ExecuteQuery($sql);

    }

    /** Lista um registro especifíco */
    function Get($content_id)
    {

        /** Parâmetro de entrada **/
        $this->content_id = (int)$content_id;

        /** Consulta SQL **/
        $sql = "select * from gn_content where content_id = {$this->content_id}";

        /** Executo o comando SQL **/
        $this->obj->ExecuteQuery($sql);

        /** Retorno em forma de objeto uma consulta SQL **/
        return $this->obj->query_fetch_object();

    }

    /** Lista todos os egistros do banco com ou sem paginação*/
    function CountBanner($organization_id)
    {

        /** Parâmetros de entrada **/
        $this->organization_id = (int)$organization_id;

        /** Consulta SQL **/
        $sql = "SELECT count(*) as qtde
                FROM gn_content gnc
                WHERE gnc.organization_id = {$this->organization_id} and gnc.highlighter = 'banner'";

        /** Executo o comando SQL */
        $this->obj->ExecuteQuery($sql);

        /** Retorno em forma de objeto uma consulta SQL **/
        return (int)$this->obj->query_fetch_object()->qtde;

    }

    /** Lista todos os egistros do banco com ou sem paginação*/
    function Banner($organization_id)
    {

        /** Parâmetros de entrada **/
        $this->organization_id = (int)$organization_id;

        /** Consulta SQL **/
        $sql = "SELECT gnc.highlighter,
                       g.content_sub_id,
                       f.name                
                FROM gn_content gnc
                JOIN gn_content_sub g ON gnc.content_id = g.content_id
                JOIN gn_content_sub_file f ON g.content_sub_id = f.content_sub_id
                WHERE organization_id = {$this->organization_id}
                AND gnc.highlighter = 'banner'";

        /** Executo o comando SQL */
        $this->obj->ExecuteQuery($sql);

    }

    /** Retorno o número de linhas de uma consulta SQL */
    function NumRow()
    {
        return $this->obj->query_num_row();
    }

    /** Libera a memória associada ao resultado */
    function FreeResult()
    {
        $this->obj->free_result();
    }

    /** Retorna a linha atual do conjunto de resultados como um objeto */
    function FetchObject()
    {
        return $this->obj->query_fetch_object();
    }

    /** Fecha uma conexão aberta anteriormente com o banco de dados */
    function __destruct()
    {
        $this->obj->close();
    }
}