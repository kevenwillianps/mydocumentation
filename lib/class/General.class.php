<?php
/**
* Classe General.class.php
* @filesource
* @autor		Kenio de Souza
* @copyright	Copyright 2017 Serenity Informatica
* @package		class
* @subpackage	class.class
* @version		1.0
* Total de campos para essa classe [16]*/


/** Configuração de acesso ao banco de dados */
require_once("lib/class/Host.php");

/** Classe de conexão ao banco de dados Mysql */
require_once("lib/class/Mysql.class.php");

class General
{

	/** Construtor da classe */
	function __construct()
	{
		/** Cria o objeto de conexão com o banco de dados */
		$this->obj = new Connect;
	}

	/** Lista os registros do banco de dados com limitação */
	function Get($organization_id)
	{

	    $this->organization_id = $organization_id;

		/** Consulta SQL */
		$sql = "select * from gn_organization 
                join gn_organization_address goa on gn_organization.organization_id = goa.organization_id
                join gn_organization_bank_account goba on gn_organization.organization_id = goba.organization_id
                join gn_organization_documentation g on gn_organization.organization_id = g.organization_id
                join gn_organization_general gog on gn_organization.organization_id = gog.organization_id
                where gn_organization.organization_id = {$this->organization_id}";

		/** Executo o comando SQL */
		$this->obj->ExecuteQuery($sql);
		
		/** Retorna o resultado*/
		return $this->obj->query_fetch_object();

	}

	/** Fecha uma conexão aberta anteriormente com o banco de dados */
	function __destruct()
	{
		$this->obj->close();
    }
}
?>