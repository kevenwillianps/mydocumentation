<?php

   /**
    * Created by MyCode
    * User: KEVEN
    * Date: 31/07/2019
    * Time: 13:32
   **/

   /** Configuração de acesso ao banco de dados **/
   require_once("lib/class/Host.php");

   /** Classe de conexão ao banco de dados Mysql **/
   require_once("lib/class/Mysql.class.php");

   class GnContact
   {

       /** Construtor da classe **/
       function __construct()
       {

           /** Cria o objeto de conexão com o banco de dados **/
           $this->obj = new Connect;

       }

       /** Lista um registro específico **/
       public function Count()
       {

           /** Consulta SQL **/
           $sql = "select count(*) as qtde from gn_contact ";

           /** Executo o comando SQL */
           $this->obj->ExecuteQuery($sql);

           /** Retorno em forma de objeto uma consulta SQL **/
           return (int)$this->obj->query_fetch_object()->qtde;

       }

       /** Listo a quantidade total de registros **/
       public function Get($contact_id)
       {

           /** Parâmetro de entrada **/
           $this->contact_id = (int)$contact_id;

           /** Consulta SQL **/
           $sql = "select * from gn_contact where contact_id = {$this->contact_id}";

           /** Executo o comando SQL **/
           $this->obj->ExecuteQuery($sql);

           /** Retorno em forma de objeto uma consulta SQL **/
           return $this->obj->query_fetch_object();

       }

       /** Lista todos os registros **/
       public function All()
       {

           /** Consulta SQL **/
           $sql = "select * from gn_contact";

           /** Executo o comando SQL **/
           $this->obj->ExecuteQuery($sql);

       }

       /** Insere/autualiza um registro no banco de dados **/
       public function Save($contact_id, $organization_id, $situation_id, $name, $email, $telephone, $cellphone, $subject_matter, $message, $date_register, $date_update)
       {

           /** Parâmetros de entrada **/
           $this->contact_id = (int)$contact_id;
           $this->organization_id = (int)$organization_id;
           $this->situation_id = (int)$situation_id;
           $this->name = (string)$name;
           $this->email = (string)$email;
           $this->telephone = (string)$telephone;
           $this->cellphone = (string)$cellphone;
           $this->subject_matter = (string)$subject_matter;
           $this->message = (string)$message;
           $this->date_register = (string)$date_register;
           $this->date_update = (string)$date_update;

           /** Verifico se é inserção ou atualização **/
           if($this->contact_id == 0)
           {

               /** Consulta SQL **/
               $sql = "INSERT INTO gn_contact(contact_id, organization_id, situation_id, name, email, telephone, cellphone, subject_matter, message, date_register, date_update)VALUES('{$this->contact_id}', '{$this->organization_id}', '{$this->situation_id}', '{$this->name}', '{$this->email}', '{$this->telephone}', '{$this->cellphone}', '{$this->subject_matter}', '{$this->message}', '{$this->date_register}', '{$this->date_update}')";

           }else
           {

               /** Consulta SQL **/
               $sql = "UPDATE gn_contact set contact_id = '{$this->contact_id}', organization_id = '{$this->organization_id}', situation_id = '{$this->situation_id}', name = '{$this->name}', email = '{$this->email}', telephone = '{$this->telephone}', cellphone = '{$this->cellphone}', subject_matter = '{$this->subject_matter}', message = '{$this->message}', date_register = '{$this->date_register}', date_update = '{$this->date_update}' WHERE contact_id = {$this->contact_id}";

           }

           /** Executo o comando SQL **/
           $this->obj->ExecuteQuery($sql);

       }

       /** Excluo um registro específico **/
       public function Delete($contact_id)
       {

           /** Parâmetro de entrada **/
           $this->contact_id = (int)$contact_id;

           /** Consulta SQL **/
           $sql = "DELETE FROM gn_contact WHERE contact_id = {$this->contact_id}";

           /** Executo o comando SQL **/
           if($this->obj->ExecuteQuery($sql))
           {

               return true;

           }else
           {

               return false;

           }

       }

       /** Retorno o número de linhas de uma consulta SQL **/
       function NumRow()
       {

           return $this->obj->query_num_row();

       }

       /** Libera a memória associada ao resultado **/
       function FreeResult()
       {

           $this->obj->free_result();

       }

       /** Retorna a linha atual do conjunto de resultados como um objeto **/
       function FetchObject()
       {

           return $this->obj->query_fetch_object();

       }

       /** Fecha uma conexão aberta anteriormente com o banco de dados **/
       function __destruct()
       {

           $this->obj->close();

       }

   }
