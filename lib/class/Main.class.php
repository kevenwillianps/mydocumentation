<?php
/**
* Classe Lista.class.php
* @filesource
* @autor		Kenio de Souza
* @copyright	Copyright 2016 Serenity Informatica
* @package		class
* @subpackage	class.class
* @version		1.0
* Total de campos para essa classe [8]*/


/** Configuração de acesso ao banco de dados */
require_once("lib/class/Host.php");

/** Classe de conexão ao banco de dados Mysql */
require_once("lib/class/Mysql.class.php");

class Main
{

	/** Cria o objeto de conexão com o banco de dados */
	private $obj;


	/** Construtor da classe */
	function __construct(){
		@ob_start();
		@session_start();	
	}

	/** Construtor da classe */
	function GuaranteedAccess()	
	{		  
	
	}
	
	/**Carrega XML*/
	function LoadXML($file)
	{
		  if($file)
		  {
			  //Recebo o xml para listagem dos dados
			  $dom = new domDocument();
			  @$dom->loadXML($file);
			  @$newxml = simplexml_import_dom($dom);	
			  
			  return $newxml;
		  }
		  
	}	

	/**Carrega Classe*/
	function LoadClass($class)
	{
		/***Instância da classe*/
		include_once($class.".class.php");
		$return = new $class();		
		
		return $return;
	}
	
	/**Trata valor para gravar no banco*/
	function MoeadDB($value)
	{
		$v = str_replace(".", "", $value);
		$v = str_replace(",", ".", $v);
		
		return $v;
		
	}
			
	//ANTIINJECTION
	function anti_injection($str)
	{
			//Check
			if(!is_array($str)){	
				
				$badchar[1] = " drop "; 
				$badchar[2] = " select "; 
				$badchar[3] = " delete "; 
				$badchar[4] = " update "; 
				$badchar[5] = " insert "; 
				$badchar[6] = " alter "; 
				$badchar[7] = " destroy "; 
				$badchar[8] = ' * ';
				$badchar[9] = " database "; 
				$badchar[10] = " drop "; 
				$badchar[11] = " union "; 
				$badchar[12] = " TABLE_NAME "; 
				$badchar[13] = " 1=1 "; 
				$badchar[14] = ' or 1 '; 
				$badchar[15] = ' exec '; 
				$badchar[16] = ' INFORMATION_SCHEMA '; 
				$badchar[17] = ' TABLE_NAME '; 
				$badchar[18] = ' like '; 
				$badchar[19] = ' COLUMNS '; 
				$badchar[20] = ' into '; 
				$badchar[21] = ' VALUES ';
				$badchar[22] = ' from '; 
				$badchar[23] = ' From ';
				$badchar[24] = ' undefined ';
				
				$y = 1; 
				$x = sizeof($badchar); 
				while ($y <= $x) 
				{ 
					 $pos = strpos(strtolower($str), strtolower($badchar[$y])); 
					 if ($pos !== false) 
					 { 
						$str = str_replace(strtolower($badchar[$y]), "", strtolower($str)); 
					 } 
					
					$y++; 
				 } 
				
				$str = trim($str);//limpa espaços vazio	
				$str= strip_tags($str);//tira tags html e php	
				$str= addslashes($str);//Adiciona barras invertidas a uma string
				$str= htmlspecialchars($str);//Evita ataques XSS
				
				return utf8_decode($str);
				
			}else{
				return $str;	
			}
	}	
	
	/**Valida CPF/CNPJ*/
	function cpfj($candidato) {
		$l = strlen($candidato = str_replace(array(".","-","/"),"",$candidato));
		if ((!is_numeric($candidato)) || (!in_array($l,array(11,14))) || (count(count_chars($candidato,1))==1)) {
			return false;
		}
		$cpfj = str_split(substr($candidato,0,$l-2));
		$k = 9;
		for ($j=0;$j<2;$j++) {
			for ($i=(count($cpfj));$i>0;$i--) {
				$s += $cpfj[$i-1] * $k;
				$k--;
				$l==14&&$k<2?$k=9:1;
			}
			$cpfj[] = $s%11==10?0:$s%11;
			$s = 0;
			$k = 9;
		}    
		return $candidato==join($cpfj);
	}		
		
	//Zeros a esquerda
	function SetZeros($valor, $qtde)
	{
	  $result = $valor;
	  $tamanho = strlen($valor);
	  $valor = "";
	  for ($i=0; $i < $qtde-$tamanho;$i++)
	  {
		$valor = "0" . $valor;
	  }
	  $result = $valor . $result;
	  return $result;
	}	
	
	//Valida emails
	// Define uma função que poderá ser usada para validar e-mails usando regexp
	function validaEmail($email) 
	{
		$er = "/^(([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}){0,1}$/";
		if (preg_match($er, $email)){
		return true;
		} else {
		return false;
		}
	}
	
	/**Trata data no banco*/
	function DataDB($value)
	{
		
		//Check
		if($value){
			$d = explode("/", $value);
			$date = $d[2].'-'.$d[1].'-'.$d[0];
			
			return $date;
		}
		
	}		
	
	//Validar data
	function validateDate($dat){
		$data = explode("/","$dat"); // fatia a string $dat em pedados, usando / como referência
		$d = $data[0];
		$m = $data[1];
		$y = $data[2];
	
		// verifica se a data é válida!
		// 1 = true (válida)
		// 0 = false (inválida)
		$return = checkdate($m,$d,$y);
		
		return $return;
	
	}	
	
	/**Limpa documento*/
	function ClearDoc($doc)
	{
			$d = str_replace(".", "", $doc);	
			$d = str_replace("/", "", $d);	
			$d = str_replace("-", "", $d);
			$d = str_replace("_", "", $d);
			$d = str_replace("(", "", $d);
			$d = str_replace(")", "", $d);
			$d = str_replace(" ", "", $d);
			
			return $d;	
	}	
	
	//COLOCA A MASCARA NO CNPF OU CNPJ
	function formatarCPF_CNPJ($campo){
	
			$tam = strlen($campo);
			
			if($tam == 11)//Verifico se é um CPF
			{
				$part1 = substr($campo, 0, 3);
				$part2 = substr($campo, 3, 3);
				$part3 = substr($campo, 6, 3);
				$part4 = substr($campo, 9, 2);
				
				$return = $part1.'.'.$part2.'.'.$part3.'-'.$part4;//Monto o cpf formatado
				
			}
			
			elseif($tam == 14)//Verifico se é um CNPJ
			{
				$part1 = substr($campo, 0, 2);
				$part2 = substr($campo, 2, 3);
				$part3 = substr($campo, 5, 3);
				$part4 = substr($campo, 8, 4);
				$part5 = substr($campo, 12, 2);
				
				$return = $part1.'.'.$part2.'.'.$part3.'/'.$part4.'-'.$part5;//Monto o cpf formatado			
			}
			
			else
			{
				$return = $campo;
			}	
			
			return $return;		
	 
	}	
	
	//Trato String
	function trataacentos($string)
	{
		  //Minusculas
		  $n = str_replace("ã", "a", $string);
		  $n = str_replace("à", "a", $n);
	  
		  $n = str_replace("á", "a", $n);
		  $n = str_replace("é", "e", $n);
		  $n = str_replace("è", "e", $n);
		  $n = str_replace("ê", "e", $n);
		  $n = str_replace("í", "i", $n);
		  $n = str_replace("î", "i", $n);
		  $n = str_replace("õ", "o", $n);
		  $n = str_replace("ó", "o", $n);
		  $n = str_replace("ò", "o", $n);
		  $n = str_replace("ô", "o", $n);
		  $n = str_replace("ú", "u", $n);
		  $n = str_replace("ù", "u", $n);
		  $n = str_replace("ç", "c", $n);
		  
		  //Maiusculas
		  $n = str_replace("Ã", "A", $n);
		  $n = str_replace("À", "A", $n);
		  $n = str_replace("Á", "A", $n);
		  $n = str_replace("É", "E", $n);
		  $n = str_replace("È", "E", $n);
		  $n = str_replace("Ê", "E", $n);
		  $n = str_replace("Í", "I", $n);
		  $n = str_replace("Î", "I", $n);
		  $n = str_replace("Õ", "O", $n);
		  $n = str_replace("Ó", "O", $n);
		  $n = str_replace("Ò", "O", $n);
		  $n = str_replace("Ô", "O", $n);
		  $n = str_replace("Ú", "U", $n);
		  $n = str_replace("Ù", "U", $n);
		  $n = str_replace("Ç", "C", $n);
		  
		  //Caracteres Especiais
		  $n = str_replace("º", " ", $n);
		  $n = str_replace("ª", " ", $n);
		  $n = str_replace("&", " ", $n);
		  
		  
		  return $n;
	}	

	/** Fecha uma conexão aberta anteriormente com o banco de dados */
	function __destruct()
	{
		
    }
}
?>