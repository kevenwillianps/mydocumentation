<?php

   /**
    * Created by MyCode
    * User: KEVEN
    * Date: 31/07/2019
    * Time: 13:32
   **/

   /** Configuração de acesso ao banco de dados **/
   require_once("lib/class/Host.php");

   /** Classe de conexão ao banco de dados Mysql **/
   require_once("lib/class/Mysql.class.php");

   class GnOrganizationAddress
   {

       /** Construtor da classe **/
       function __construct()
       {

           /** Cria o objeto de conexão com o banco de dados **/
           $this->obj = new Connect;

       }

       /** Lista um registro específico **/
       public function Count()
       {

           /** Consulta SQL **/
           $sql = "select count(*) as qtde from gn_organization_address ";

           /** Executo o comando SQL */
           $this->obj->ExecuteQuery($sql);

           /** Retorno em forma de objeto uma consulta SQL **/
           return (int)$this->obj->query_fetch_object()->qtde;

       }

       /** Listo a quantidade total de registros **/
       public function Get($organization_address_id)
       {

           /** Parâmetro de entrada **/
           $this->organization_address_id = (int)$organization_address_id;

           /** Consulta SQL **/
           $sql = "select * from gn_organization_address where organization_address_id = {$this->organization_address_id}";

           /** Executo o comando SQL **/
           $this->obj->ExecuteQuery($sql);

           /** Retorno em forma de objeto uma consulta SQL **/
           return $this->obj->query_fetch_object();

       }

       /** Lista todos os registros **/
       public function All($organization_id)
       {

           /** Parâmetro de entrada **/
           $this->organization_id = (int)$organization_id;

           /** Consulta SQL **/
           $sql = "select * from gn_organization_address WHERE organization_id = {$this->organization_id}";

           /** Executo o comando SQL **/
           $this->obj->ExecuteQuery($sql);

       }

       /** Insere/autualiza um registro no banco de dados **/
       public function Save($organization_address_id, $situation_id, $organization_id, $zip_code, $complement, $number, $residence_time, $uf, $city, $date_register, $date_update)
       {

           /** Parâmetros de entrada **/
           $this->organization_address_id = (int)$organization_address_id;
           $this->situation_id = (int)$situation_id;
           $this->organization_id = (int)$organization_id;
           $this->zip_code = (string)$zip_code;
           $this->complement = (string)$complement;
           $this->number = (int)$number;
           $this->residence_time = (int)$residence_time;
           $this->uf = (string)$uf;
           $this->city = (string)$city;
           $this->date_register = (string)$date_register;
           $this->date_update = (string)$date_update;

           /** Verifico se é inserção ou atualização **/
           if($this->organization_address_id == 0)
           {

               /** Consulta SQL **/
               $sql = "INSERT INTO gn_organization_address(organization_address_id, situation_id, organization_id, zip_code, complement, number, residence_time, uf, city, date_register, date_update)VALUES('{$this->organization_address_id}', '{$this->situation_id}', '{$this->organization_id}', '{$this->zip_code}', '{$this->complement}', '{$this->number}', '{$this->residence_time}', '{$this->uf}', '{$this->city}', '{$this->date_register}', '{$this->date_update}')";

           }else
           {

               /** Consulta SQL **/
               $sql = "UPDATE gn_organization_address set organization_address_id = '{$this->organization_address_id}', situation_id = '{$this->situation_id}', organization_id = '{$this->organization_id}', zip_code = '{$this->zip_code}', complement = '{$this->complement}', number = '{$this->number}', residence_time = '{$this->residence_time}', uf = '{$this->uf}', city = '{$this->city}', date_register = '{$this->date_register}', date_update = '{$this->date_update}' WHERE organization_address_id = {$this->organization_address_id}";

           }

           /** Executo o comando SQL **/
           $this->obj->ExecuteQuery($sql);

       }

       /** Excluo um registro específico **/
       public function Delete($organization_address_id)
       {

           /** Parâmetro de entrada **/
           $this->organization_address_id = (int)$organization_address_id;

           /** Consulta SQL **/
           $sql = "DELETE FROM gn_organization_address WHERE organization_address_id = {$this->organization_address_id}";

           /** Executo o comando SQL **/
           if($this->obj->ExecuteQuery($sql))
           {

               return true;

           }else
           {

               return false;

           }

       }

       /** Retorno o número de linhas de uma consulta SQL **/
       function NumRow()
       {

           return $this->obj->query_num_row();

       }

       /** Libera a memória associada ao resultado **/
       function FreeResult()
       {

           $this->obj->free_result();

       }

       /** Retorna a linha atual do conjunto de resultados como um objeto **/
       function FetchObject()
       {

           return $this->obj->query_fetch_object();

       }

       /** Fecha uma conexão aberta anteriormente com o banco de dados **/
       function __destruct()
       {

           $this->obj->close();

       }

   }
