<?php
/**
* Classe ContentSub.class.php
* @filesource
* @autor		Kenio de Souza
* @copyright	Copyright 2017 Serenity Informatica
* @package		class
* @subpackage	class.class
* @version		1.0
* Total de campos para essa classe [12]*/

/** Configuração de acesso ao banco de dados */
require_once("lib/class/Host.php");

/** Classe de conexão ao banco de dados Mysql */
require_once("lib/class/Mysql.class.php");

class ContentSub
{

	/** Construtor da classe */
	function __construct()
	{

		/** Cria o objeto de conexão com o banco de dados */
		$this->obj = new Connect;

	}

    /** Lista um registro especifíco */
    function Get($content_sub_id)
    {

        /** Parâmetro de entrada **/
        $this->content_sub_id = (int)$content_sub_id;

        /** Consulta SQL **/
        $sql = "select * from gn_content_sub where content_sub_id = {$this->content_sub_id}";

        /** Executo o comando SQL **/
        $this->obj->ExecuteQuery($sql);

        /** Retorno em forma de objeto uma consulta SQL **/
        return $this->obj->query_fetch_object();

    }

    /** Lista os registros do banco de dados com limitação */
    function Load($content_id)
    {

        /** Parâmetros de entrada **/
        $this->content_id = (int)$content_id;

        /** Consulta SQL */
        $sql = "select gncs.content_sub_id,
                       gncs.title,
                       gncs.title_menu,
                       gncs.content_observation,
                       gncs.content_resume,
                       gncs.content_complete,
                       g.name
                from gn_content_sub gncs
                left join gn_content_sub_file g on gncs.content_sub_id = g.content_sub_id
                where gncs.content_id = {$this->content_id}";

        /** Executo o comando SQL */
        $this->obj->ExecuteQuery($sql);

    }

    /** Lista os registros do banco de dados com limitação */
    function Auxiliary($content_sub_id)
    {

        /** Parâmetros de entrada **/
        $this->content_sub_id = (int)$content_sub_id;

        /** Consulta SQL */
        $sql = "select gncs.content_sub_id,
                       gncs.title,
                       gncs.title_menu,
                       gncs.content_observation,
                       gncs.content_resume,
                       gncs.content_complete,
                       g.name
                from gn_content_sub gncs
                left join gn_content_sub_file g on gncs.content_sub_id = g.content_sub_id
                where gncs.content_sub_auxiliary_id = {$this->content_sub_id}";

        /** Executo o comando SQL */
        $this->obj->ExecuteQuery($sql);

    }

    /** Conta a quantidades de registros */
	function Count($content_id)
	{

		/** Parâmetros de entrada **/
		$this->content_id = (int)$content_id;

		/** Consulta SQL */
		$sql = "select count(content_sub_id) as qtde
				from gn_content_sub 
				where content_id = {$this->content_id}";

		/** Executo o comando SQL */
		$this->obj->ExecuteQuery($sql);
		return (int)$this->obj->query_fetch_object()->qtde;

	}

    /** Retorno o número de linhas de uma consulta SQL */
	function NumRow()
	{
		return $this->obj->query_num_row();	
	}

	/** Libera a memória associada ao resultado */
	function FreeResult()
	{
		$this->obj->free_result();
	}		

	/** Retorna a linha atual do conjunto de resultados como um objeto */
	function FetchObject()
	{
		return $this->obj->query_fetch_object();
	}

    /** Retorna a linha atual do conjunto de resultados como um objeto */
    function FetchAssoc()
    {
        return $this->obj->query_fetch_row();
    }

	/** Fecha uma conexão aberta anteriormente com o banco de dados */
	function __destruct()
	{
		$this->obj->close();
    }
}
?>